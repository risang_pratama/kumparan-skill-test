package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/olivere/elastic/v7"
	"github.com/streadway/amqp"
)

// News for return result from MySQL
type News struct {
	ID      string `form:"id"`
	Author  string `form:"author"`
	Body    string `form:"body"`
	Created string `form:"created"`
}

// NewsES For Elasticsearch
type NewsES struct {
	NewsID  int    `json:"news_id"`
	Created string `json:"created"`
}

// NewsReq for Get Request Body
type NewsReq struct {
	Author string `json:"author"`
	Body   string `json:"body"`
}

// Response for return result from endpoint
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    []News
}

func connectDB() *sql.DB {
	db, err := sql.Open("mysql", "root:12345678@tcp(localhost:3306)/kumparan")

	if err != nil {
		log.Fatal(err)
	}

	return db
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func listNews(w http.ResponseWriter, r *http.Request) {
	var page int
	var offset int
	var news News
	var arrNews []News
	var response Response
	var wg sync.WaitGroup

	key := "kumparan_news"
	e := `"` + key + `"`
	w.Header().Set("Etag", e)
	w.Header().Set("Cache-Control", "max-age=86400")

	if match := r.Header.Get("If-None-Match"); match != "" {
		if strings.Contains(match, e) {
			w.WriteHeader(http.StatusNotModified)
			return
		}
	}

	ctx := context.Background()
	elasticClient, err := elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"))
	if err != nil {
		fmt.Println(err.Error())
	}

	params, ok := r.URL.Query()["page"]
	if !ok || len(params[0]) < 1 {
		page = 1
	} else {
		pageint, _ := strconv.Atoi(params[0])
		page = pageint
	}
	limit := 10
	offset = (page - 1) * limit

	query := elastic.MatchAllQuery{}
	searchResult, err := elasticClient.Search().Index("news").
		Query(query).
		Sort("created", false).
		From(offset).Size(limit).
		Do(ctx)

	if searchResult.TotalHits() > 0 {
		db := connectDB()
		defer db.Close()

		for _, hit := range searchResult.Hits.Hits {
			var n NewsES
			err := json.Unmarshal(hit.Source, &n)
			if err != nil {
				log.Printf("Can't deserialize 'news' object : %s", err.Error())
			}

			wg.Add(1)
			go func() {
				defer wg.Done()

				sqlStatement := `SELECT id, author, body, created FROM news WHERE id=?;`
				row := db.QueryRow(sqlStatement, n.NewsID)
				errRow := row.Scan(&news.ID, &news.Author, &news.Body, &news.Created)
				if errRow != nil {
					log.Fatal(errRow.Error())
				} else {
					arrNews = append(arrNews, news)
				}
			}()
			wg.Wait()
		}
	}

	response.Status = "ok"
	response.Data = arrNews

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func createNews(w http.ResponseWriter, r *http.Request) {
	var newsReq NewsReq
	var response Response

	_ = json.NewDecoder(r.Body).Decode(&newsReq)

	var created = time.Now().Format(time.RFC3339)

	news := News{"", newsReq.Author, newsReq.Body, created}

	sendMessage(news)

	response.Status = "ok"
	response.Message = "News Successfully Saved"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func sendMessage(news News) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect AMQP")

	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to connect Channel")

	defer ch.Close()

	q, err := ch.QueueDeclare(
		"news",
		true,
		false,
		false,
		false,
		nil,
	)

	failOnError(err, "Failed to declare a queue")

	msg, err := json.Marshal(news)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(msg))

	err = ch.Publish(
		"newsNotifExchange",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(string(msg)),
		})

	log.Printf("Message: %s", msg)
	failOnError(err, "Failed to publish a message")

}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/news", createNews).Methods("POST")
	router.HandleFunc("/news", listNews).Methods("GET")

	log.Fatal(http.ListenAndServe(":1234", router))
}
