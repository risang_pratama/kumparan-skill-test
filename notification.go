package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/olivere/elastic/v7"
	"github.com/streadway/amqp"
)

// News For Retrieve Value from Message
type News struct {
	Author  string `json:"author"`
	Body    string `json:"body"`
	Created string `json:"created"`
}

// NewsOBJ For Save to Elasticsearch
type NewsOBJ struct {
	NewsID  int64  `json:"news_id"`
	Created string `json:"created"`
}

func connectDB() *sql.DB {
	db, err := sql.Open("mysql", "root:12345678@tcp(localhost:3306)/kumparan")

	if err != nil {
		log.Fatal(err)
	}

	return db
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func receiveMessage(ctx context.Context, elasticClient *elastic.Client) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect AMQP")

	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"news",
		true,
		false,
		false,
		false,
		nil,
	)

	failOnError(err, "Failed to declare a queue")

	messages, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		db := connectDB()
		defer db.Close()

		for d := range messages {
			msg := string(d.Body)
			news := News{}

			json.Unmarshal([]byte(msg), &news)

			res, err := db.Exec("INSERT INTO news (author, body, created) values (?,?,?)", news.Author, news.Body, news.Created)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				id, err := res.LastInsertId()
				if err != nil {
					fmt.Println("Error:", err.Error())
				} else {
					newsOBJ := NewsOBJ{id, news.Created}
					fmt.Println(news.Created)

					_, err := elasticClient.Index().Index("news").BodyJson(newsOBJ).Do(ctx)
					if err != nil {
						fmt.Printf("NewsId=%d was not created. Error : %s \n", newsOBJ.NewsID, err.Error())
					}
				}
			}
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func main() {
	ctx := context.Background()

	// init Elastic client
	elasticClient, err := elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"))
	if err != nil {
		fmt.Println(err.Error())
	}

	receiveMessage(ctx, elasticClient)
}
